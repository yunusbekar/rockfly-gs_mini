//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Victor Tchistiak - 2019
//
//This example demostrates master mode bluetooth connection and pin 
//it creates a bridge between Serial and Classical Bluetooth (SPP)
//this is an extention of the SerialToSerialBT example by Evandro Copercini - 2018
//

#include "BluetoothSerial.h"
#include <ArduinoJson.h>

// compute the required size
const size_t CAPACITY = JSON_ARRAY_SIZE(1400);

BluetoothSerial SerialBT;

String MACadd = "AA:BB:CC:11:22:33";
uint8_t address[6]  = {0xAA, 0xBB, 0xCC, 0x11, 0x22, 0x33};
//uint8_t address[6]  = {0x00, 0x1D, 0xA5, 0x02, 0xC3, 0x22};
String name = "ESP32test";
char *pin = "1234"; //<- standard pin would be provided by default
bool connected;

StaticJsonDocument<1500> doc;

byte bytearray[150];

struct Data{
byte byteArray[150];
byte tr_array[10];
int counter=0;
};
Data data;

//byte to functions
uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
long byteToLong(byte *byterray, int *count) {
  long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToULong(byte *byterray, int *count) {
  unsigned long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt16(byte *byterray, int *count) {
  int16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToUint16(byte *byterray, int *count) {
  uint16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
//byte conversion functions
void floatAddByte(float data, int *count, byte *array) {
  for (int m = 0; m < 4; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void longAddByte(long data, int *count, byte *array) {
  for (int m = 0; m < 4; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void unLongAddByte(unsigned long data, int *count, byte *array) {
  for (int m = 0; m < 4; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void int16AddByte(int16_t data, int *count, byte *array) {
  for (int m = 0; m < 2; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void unint16AddByte(uint16_t data, int *count, byte *array) {
  for (int m = 0; m < 2; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void uint8AddByte(uint8_t data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}

void setup() {
  Serial.begin(115200);
  //SerialBT.setPin(pin);
  SerialBT.begin("ESP32test1", true); 
  //SerialBT.setPin(pin);
  Serial.println("The device started in master mode, make sure remote BT device is on!");
  
  // connect(address) is fast (upto 10 secs max), connect(name) is slow (upto 30 secs max) as it needs
  // to resolve name to address first, but it allows to connect to different devices with the same name.
  // Set CoreDebugLevel to Info to view devices bluetooth address and device names
  connected = SerialBT.connect(name);
  //connected = SerialBT.connect(address);
  
  if(connected) {
    Serial.println("Connected Succesfully!");
  } else {
    while(!SerialBT.connected(10000)) {
      Serial.println("Failed to connect. Make sure remote device is available and in range, then restart app."); 
    }
  }

  
   //JsonArray bytearray = doc.createNestedArray("bytearray"); 

// add some values
/*  abc.add("hello");
  abc.add(42);
  abc.add(3.14);*/

//a=1;
//}

  
   //Serial.println(bytearray.memoryUsage());  // 16 on AVR
   //Serial.println(doc.memoryUsage()); 
    
  // disconnect() may take upto 10 secs max
  /*if (SerialBT.disconnect()) {
    Serial.println("Disconnected Succesfully!");
  }*/
  // this would reconnect to the name(will use address, if resolved) or address used with connect(name/address).
  SerialBT.connect();
}
int a;
void loop() {

 /* if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }*/

  /* if(a==1){
   Serial.println("1.");
   data.counter=0;
   byteAddByte(0x60, &data.counter, data.byteArray);
   byteAddByte(0x21, &data.counter, data.byteArray);

   
   int16AddByte(0.16 * 100, &data.counter, data.byteArray);
   int16AddByte(0.17 * 100, &data.counter, data.byteArray);
   int16AddByte(0.18 * 100, &data.counter, data.byteArray);

   int16AddByte(35.20 * 100, &data.counter, data.byteArray);
   floatAddByte(3000, &data.counter, data.byteArray);
   floatAddByte(5.20, &data.counter, data.byteArray);


   int16AddByte(0.16 * 100, &data.counter, data.byteArray);
   int16AddByte(0.17 * 100, &data.counter, data.byteArray);
   int16AddByte(0.18 * 100, &data.counter, data.byteArray);

   int16AddByte(100 * 100, &data.counter, data.byteArray);
   int16AddByte(200 * 100, &data.counter, data.byteArray);
   int16AddByte(300 * 100, &data.counter, data.byteArray);

   int16AddByte(5.2 * 100, &data.counter, data.byteArray);
   int16AddByte(6.2 * 100, &data.counter, data.byteArray);
   int16AddByte(7.2 * 100, &data.counter, data.byteArray);

   int16AddByte(180 * 100, &data.counter, data.byteArray);
   int16AddByte(90 * 100, &data.counter, data.byteArray);
   int16AddByte(0 * 100, &data.counter, data.byteArray);

   longAddByte(40123456, &data.counter, data.byteArray);
   longAddByte(29123456, &data.counter, data.byteArray);
   longAddByte(3000, &data.counter, data.byteArray);

   //int16AddByte(1000 * 100, &data.counter, data.byteArray);

   //longAddByte(123456, &data.counter, data.byteArray);

   int16AddByte(3.7 * 100, &data.counter, data.byteArray);
 
   byteAddByte(12, &data.counter, data.byteArray);

   byteAddByte(4, &data.counter, data.byteArray);
   data.counter=0;
a=0;
}*/


  
//if(a==0){
 // Serial.println("2.");
  /*   data.counter=0;
   byteAddByte(0x60, &data.counter, data.byteArray);
   byteAddByte(0x21, &data.counter, data.byteArray);

   
   int16AddByte(1.16 * 100, &data.counter, data.byteArray);
   int16AddByte(2.17 * 100, &data.counter, data.byteArray);
   int16AddByte(3.18 * 100, &data.counter, data.byteArray);

   int16AddByte(20.00 * 100, &data.counter, data.byteArray);
   floatAddByte(500, &data.counter, data.byteArray);
   floatAddByte(2.30, &data.counter, data.byteArray);


   int16AddByte(1.16 * 100, &data.counter, data.byteArray);
   int16AddByte(2.17 * 100, &data.counter, data.byteArray);
   int16AddByte(3.18 * 100, &data.counter, data.byteArray);

   int16AddByte(200 * 100, &data.counter, data.byteArray);
   int16AddByte(100 * 100, &data.counter, data.byteArray);
   int16AddByte(100 * 100, &data.counter, data.byteArray);

   int16AddByte(8.2 * 100, &data.counter, data.byteArray);
   int16AddByte(9.2 * 100, &data.counter, data.byteArray);
   int16AddByte(0.2 * 100, &data.counter, data.byteArray);

   int16AddByte(150 * 100, &data.counter, data.byteArray);
   int16AddByte(45 * 100, &data.counter, data.byteArray);
   int16AddByte(25 * 100, &data.counter, data.byteArray);

   longAddByte(51123478, &data.counter, data.byteArray);
   longAddByte(30123498, &data.counter, data.byteArray);
   longAddByte(1000, &data.counter, data.byteArray);

   //int16AddByte(1000 * 100, &data.counter, data.byteArray);

   //longAddByte(123456, &data.counter, data.byteArray);

   int16AddByte(4.2 * 100, &data.counter, data.byteArray);
 
   byteAddByte(a, &data.counter, data.byteArray);

   byteAddByte(28, &data.counter, data.byteArray);
   data.counter=0;



  for(int i=0;i<150;i++){
   doc["bytearray"][i]=data.byteArray[i]; 
   }*/
/*
 doc["CONFIG"]["FRQ"] = 868.50;
 doc["CONFIG"]["DBM"] = 22;
 doc["CONFIG"]["PRSHT"] = 600;
 doc["CONFIG"]["FLSH_CLEAN"] = 0;
 doc["CONFIG"]["STATUS"] = 0;
 doc["CONFIG"]["UNIT"] = "METRIC";
 

 doc["ROCKET"]["H3LIS331"]["ACCEL_X"]=1.26;
 doc["ROCKET"]["H3LIS331"]["ACCEL_Y"]=1.26;
 doc["ROCKET"]["H3LIS331"]["ACCEL_Z"]=1.26;

 doc["ROCKET"]["MS5611"]["ALTITUDE"]=500;
 doc["ROCKET"]["MS5611"]["TEMP"]=32.26;
 doc["ROCKET"]["MS5611"]["SPEED"]=50.26;

 doc["ROCKET"]["FLIGHT"]["ID"]=1;
 doc["ROCKET"]["FLIGHT"]["MODE"]=0;

 doc["ROCKET"]["BNO055"]["ACCEL_X"]=2.23;
 doc["ROCKET"]["BNO055"]["ACCEL_Y"]=3.25;
 doc["ROCKET"]["BNO055"]["ACCEL_Z"]=5.5;

 doc["ROCKET"]["BNO055"]["GYRO_X"]=100;
 doc["ROCKET"]["BNO055"]["GYRO_Y"]=200;
 doc["ROCKET"]["BNO055"]["GYRO_Z"]=300;

 doc["ROCKET"]["BNO055"]["MAG_X"]=25.5;
 doc["ROCKET"]["BNO055"]["MAG_Y"]=30.26;
 doc["ROCKET"]["BNO055"]["MAG_Z"]=-20.26;

 doc["ROCKET"]["BNO055"]["YAW"]=45;
 doc["ROCKET"]["BNO055"]["PITCH"]=90;
 doc["ROCKET"]["BNO055"]["ROLL"]=180;

 doc["ROCKET"]["GPS"]["LAT"]=40.123456;
 doc["ROCKET"]["GPS"]["LONG"]=29.123456;
 doc["ROCKET"]["GPS"]["SAT"]=12;
 doc["ROCKET"]["GPS"]["SPEED"]=150;

 doc["ROCKET"]["BATTERY"]["ROCKET"] = 3.7;
 doc["ROCKET"]["BATTERY"]["GS"] = 4.2;

 doc["ROCKET"]["SD_FREE"] = 30;


 doc["ROCKET"]["LORA"]["RSSI"] = -39.01;

 
 doc["ROCKET"]["PYRO"]["APOGEE"]=1;
 doc["ROCKET"]["PYRO"]["MAIN"]=1;


 doc["FLIGHT_STATISTICS"]["CRC_ERR"] = 1;// istatistğe geç

 doc["FLIGHT_STATISTICS"]["MAX_ALTITUDE"]=3000;
 doc["FLIGHT_STATISTICS"]["MAX_SPEED"]=163;

 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_X"]=12.5;
 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_Y"]=15.2;
 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_Z"]=8.8;

 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_X"]=12.5;
 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_Y"]=15.2;
 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_Z"]=8.8;
 
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_X"]=12.5;
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_Y"]=15.2;
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_Z"]=8.8;
 
 doc["FLIGHT_STATISTICS"]["MAX_SATALITE"]=12;
 doc["FLIGHT_STATISTICS"]["ENGINE_BURN_TIME"]=2;
 doc["FLIGHT_STATISTICS"]["APOGEE_TIME"]=19;
 doc["FLIGHT_STATISTICS"]["APOGEE_FALL_SPEED"]=25;
 doc["FLIGHT_STATISTICS"]["MAIN_FALL_SPEED"]=9;
 doc["FLIGHT_STATISTICS"]["DESCENT_TIME"]=45;
 doc["FLIGHT_STATISTICS"]["PYRO_BURN_TIME"]=0.12;
 doc["FLIGHT_STATISTICS"]["FLIGHT_TIME"]=64;
 doc["FLIGHT_STATISTICS"]["MACH_LOCK"]=0;
 doc["FLIGHT_STATISTICS"]["FLSH_FREE"]=65;
 
*/


/*
 doc["H3LIS331_X"] = 1.26;
 doc["H3LIS331_Y"] = 2.51;
 doc["H3LIS331_Z"] = 9.10;
 
 doc["Altitude"] = 500.60;
 doc["TEMPRATURE"] = 32.5;
 doc["SPEED"] = 50;
 
 doc["BNO_ACCEL_X"] = 1.26;
 doc["BNO_ACCEL_Y"] = 2.51;
 doc["BNO_ACCEL_Z"] = 9.20;

 doc["BNO_GYRO_X"] = 50;
 doc["BNO_GYRO_Y"] = 60;
 doc["BNO_GYRO_Z"] = 70;

 doc["BNO_MAG_X"] = 100;
 doc["BNO_MAG_Y"] = 200;
 doc["BNO_MAG_Z"] = 300;
 
 doc["BNO_YAW"] = 45;
 doc["BNO_PITCH"] = 90;
 doc["BNO_ROLL"] = 180;

 doc["GPS_LAT"] = 40.123456;
 doc["GPS_LONG"] = 29.123456;
 doc["GPS_SAT"] = a;
 
 doc["BATTERY"] = 4.2;
 
 doc["PYRO 1"] = 1;
 doc["PYRO 2"] = 0;
Serial.println(doc.memoryUsage()); */

      if (SerialBT.available() ) {
      DeserializationError error = deserializeJson(doc, SerialBT);
       serializeJsonPretty(doc, Serial);
          if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
            return;
     }
  }
  

 //serializeJson(doc, SerialBT);
// delay(50);
 //a++;
  
}
