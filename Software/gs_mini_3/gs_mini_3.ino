#include <TJpg_Decoder.h>
#include <Wire.h>
#include <EEPROM.h>
#include <SimpleKalmanFilter.h>


#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
#include <ArduinoJson.h>

// compute the required size
const size_t CAPACITY = JSON_ARRAY_SIZE(1300);
StaticJsonDocument<1300> doc;

SimpleKalmanFilter simpleKalmanFilter(2, 2, 0.1);


#include "LITTLEFS.h" 

#include <RadioLib.h>
#include "SPI.h"
SPIClass abc(HSPI);
SX1262 radio = new Module(15, 17, 27, 39,abc);


TaskHandle_t Task1;



void listDir(fs::FS &fs, const char * dirname, uint8_t levels);
void deleteFile(fs::FS &fs, const char * path);
void renameFile(fs::FS &fs, const char * path1, const char * path2);
void appendFile(fs::FS &fs, const char * path, String message);
void writeFile(fs::FS &fs, const char * path, const char * message);
void readFile(fs::FS &fs, const char * path);
void removeDir(fs::FS &fs, const char * path);
void createDir(fs::FS &fs, const char * path);



float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  int16_t ByteToint16(byte *byterray,int ca)
{
  int16_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
 
return f;
  }

    uint16_t ByteTouint16(byte *byterray,int ca)
{
  uint16_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
 
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

   unsigned long ByteTounsignedlong(byte *byterray,int ca)
{
 unsigned long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

 int ByteToint(byte *byterray,int ca)
{
  int f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

   int ByteTobyte(byte *byterray,int ca)
{
  byte f;
  ((uint8_t*)&f)[0]=byterray[ca];
return f;
  }

/*    uint8_t ByteTouint8t(byte *byterray,int ca)
{
  uint8_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }*/

  void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}

void uint8tAddByte(uint8_t data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}



uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t* )&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
long byteToLong(byte *byterray, int *count) {
  long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t* )&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToULong(byte *byterray, int *count) {
  unsigned long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt16(byte *byterray, int *count) {
  int16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToUint16(byte *byterray, int *count) {
  uint16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t* )&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}




float flat1;  
float flon1;
float dist_calc=0;
float dist_calc2=0;
float diflat=0;
float diflon=0;
float x2lat;
float x2lon;


struct flightdata{
  
float H3LIS331_data[3];
float Ms5611_data[4];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
float MAX_ALT;
float GPS_speed;

uint16_t APOGEE_TIME;
uint16_t DESCENT_TIME;
uint8_t ENGINEBURN_TIME;
float PYROBURN_TIME;
float MAX_ACCEL[3];
float MAX_SPEED;
float APOGEE_FALL_SPEED;
float MAIN_FALL_SPEED;
long GPS_data[3];
uint8_t SIV;
uint8_t MAX_SIV;
uint8_t mach_lock;
long unix_time;
float batteryy;
float RISE_ACC[3];
float FALL_ACC[3];
long deneme;
long pyro_burn_time;
float rocket_flash;
};
flightdata flightData;
flightdata flightstatMax;
flightdata flightstatMin;

unsigned long FLIGHT_TIME;
int compmin_x,compmin_y,compmin_z,compmax_x,compmax_y,compmax_z;
int screen_id,k,i,kts,t,v1,d,v2,u;
float apogee=500;
float main=500;
uint32_t a,b;
char *unit[] ={"m","i","M","m/s","km/h","C","ft","ft/s","mph","F"}; 
char buf[40];
float my_temp,my_alt,rckt_temp,rckt_alt,rckt_v1,rckt_v2,rckt_dist;



uint32_t tt=0;

float aa,cardSize,cardused;
long l;
int ii,c,m,last_st;
bool kk,ok,flg,comm,bl_rqst,alt_calc=0,snd_status=0,prsht_succes=0;
char veri[] = "";

int fake_alt;
bool plus=0;

float last_frq,frquency_check;
int last_dbm,dbm_check,sd_cap,grnd_alt;
float frq=868.00;
int frq_ktss;
int dbm=20;
float frq_kts=0.1;

float carrierFreq;
uint8_t outputPower;
uint8_t pyro_stat=0;
bool pyro_stat_arr[8];
bool tch_grnd=0,header_count=1,gps_cnt=1,tst=1,flash_warning,flash_succes,wrng,flash_error,new_frq,get_frq_data,let_send,cls=0;

int rocketapoge;
float rocketmain;

byte passw = 0x60;
byte getsensData = 0x20;
byte getflightData= 0x21;
byte getsensData_flgtstcs= 0x22;
byte getsensData_tch_grnd= 0x23;
byte getsensData_tch_grnd_all= 0x24;

byte send_prsht_config = 0x50;
byte get_prsht_config  = 0x51;
byte send_frq_config = 0x30;
byte get_frq_config = 0x31; 


byte pyro_drg_config = 0x70;
byte pyro_main_config = 0x71;

byte flash_erase = 0x80;

byte check_frq = 0x40;
byte setConfig = 0x10;

byte drg_prst_time=0;
byte main_prst_time=0;


byte flight_mode=0;

bool drg_prst_count=0;
bool main_prst_count=0;

int pyro1,pyro2;

bool bt_send_data_after_flight=0,bt_send_flight=0;
bool bt_send=0;
bool bt_send_sttcs=0;


unsigned long time_gpsfix=0;

unsigned long timeout_hz=0;
unsigned long timeout=0;
unsigned long timeout_sens=0;
unsigned long timeout_fall=0;
unsigned long timeout_comm=0;
unsigned long timeout_flash=0;
unsigned long timeout_bluetooth=0;
unsigned long timeout_prsht=0;

unsigned long drg_prst_timer=0;
unsigned long main_prst_timer=0;

int transmissionState = ERR_NONE;

struct Data{
byte byteArray[200];
byte tr_array[10];
int counter=0;
};
Data data;

int fake_mode=1;

String s1 ={"!"};
byte btarray[1];

char strr[210];
String All_data2;
String header = "GpsTime,200gAccX,200gAccY,200gAccZ,Temp,Alt,Speed,BNOAccX,BNOAccY,BNOAccZ,BNOGyrX,BNOGyrY,BNOGyrZ,BNOMagX,BNOMagY,BNOMagZ,BNOYaw,BNOPitch,BNORoll,GpsLat,GpsLong,GpsAlt,GpsSpeed,SIV,Batt,PyroDraque,PyroMain,MAX_ALTITUDE,MAX_SPEED,MAX_ACCEL_X,MAX_ACCEL_Y,MAX_ACCEL_Z,RISE_ACC_X,RISE_ACC_Y,RISE_ACC_Z,FALL_ACC_X,FALL_ACC_Y,FALL_ACC_Z,MAX_SIV,ENGINE_BURN_TIME,PYRO_BURN_TIME,APOGEE_TIME,APOGEE_FALL_SPEED,MAIN_FALL_SPEED,DESCENT_TIME,FLIGHT_TIME,UnixTIME\n";
String All_data1;
int All_dataa,cnt;
int frst_cnt,stt=0,ct=0;
int crc_err,compare=2;
byte v=0,selffrq_set=0;

bool try_com,check_data,time_com,rqst_err,snd_err,prsht_rqst_err,prsht_snd_err,config_page,pyro_succes=0,again_prsht_rqst=1;

#define INITIATING_NODE = true;
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}



void setup() {
   Serial.begin(115200); 


   pinMode( 26, OUTPUT);
if (!SD.begin()) {
   // Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
//Serial.println("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
    Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);


  xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    40000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 

 
                u=0;
                d=2;
                v1=3;
                v2=4;
                t=5;

  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");

}


// flag to indicate transmission or reception state

void Task1code( void * pvParameters ){

  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  abc.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
 
  EEPROM.begin(512);
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());

 //// Serial.printf("SD Card Size: %lluMB\n", cardSize);

 
 /*preferences.begin("my-config", false);
 
  frquency_check=preferences.getFloat("frq",0);
  Serial.print(frquency_check);
  if(frquency_check==0)frq=868.00;
  else frq=frquency_check;
  Serial.print(" frekans= ");
  Serial.print(frq);

  dbm_check=preferences.getInt("dbm",0);
  Serial.print(dbm_check);
  if(dbm_check==0)dbm=20;
  else dbm=dbm_check;
  Serial.print(" dbm= ");
  Serial.print(dbm);*/

  
 // EEPROM.writeFloat(0,frq);
 // EEPROM.writeInt(6,dbm);
 // EEPROM.commit();
 //EEPROM.writeFloat(0,frq);
  frquency_check=EEPROM.readFloat(0);
  Serial.print(" içindeki frekans ");
  Serial.print(frquency_check);
  if(frquency_check>=868 && frquency_check<=915)frq=frquency_check;
  else frq=868.00;
  Serial.print(" frekans= ");
  Serial.println(frq);

  dbm_check=EEPROM.readInt(6);
  Serial.print(" içindeki dbm ");
  Serial.print(dbm_check);
  if(dbm_check<=0 || dbm_check>=23)dbm=20;
  else dbm=dbm_check;
  Serial.print(" dbm= ");
  Serial.println(dbm);


int state = radio.begin(frq, 500.0, 7, 5, 0x34,dbm);
  if (state == ERR_NONE) {
   // Serial.println(F("success!"));
  } else {
   // Serial.print(F("failed, code "));
   // Serial.println(state);
    while (true);
  }
radio.setCurrentLimit(110);
  // set the function that will be called
  // when new packet is received
 radio.setDio1Action(setFlag);
  #if defined(INITIATING_NODE)
    // send the first packet on this node
    Serial.print(F("[SX1262] Sending first packet ... "));
    transmissionState = radio.startTransmit(data.tr_array,10);
    transmitFlag = true;
  #else
    // start listening for LoRa packets on this node
    Serial.print(F("[SX1262] Starting to listen ... "));
    state = radio.startReceive();
    if (state == ERR_NONE) {
      Serial.println(F("success!"));
    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);
      while (true);
    }
  #endif

  // if needed, 'listen' mode can be disabled by calling
  // any of the following methods:
  //
  // radio.standby()
  // radio.sleep()
  // radio.transmit();
  // radio.receive();
  // radio.readData();
  // radio.scanChannel();

 
  for(;;){

   // timeout_hz=millis();
    vTaskDelay(1);
    
/*cardSize = SD.cardSize() / (1024 * 1024);
cardused = SD.usedBytes() / (1024 * 1024);
cardSize=cardSize/1000;
cardused=cardused/1000;



sd_cap=(100*cardused)/cardSize;*/








  if(flight_mode==0){
    
  if(millis()-timeout_sens > 5000){
    
      kk=0;
      enableInterrupt = false;
     // Serial.println(" roketten sensör data istendi ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(getsensData, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      //check_data=0;  
      enableInterrupt = true;
     timeout_sens = millis();
     

}
  }

      // burada rqst butonuna basılırsa değişken aktif olup config isteme gönderme işlemi yapıyor.
  /*  if(stt==1){
      kk=0;
      enableInterrupt = false;
      Serial.println(" roketten frq config istendi ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_frq_config, &data.counter, data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
       for(int p =0;p<2;p++){
        Serial.print(data.tr_array[p],HEX);
        Serial.print(",");
        
      }
      Serial.println("");
      transmitFlag = true;
      stt=0;
      //check_data=0;  
      enableInterrupt = true;

}*/
    
     // burada send butonuna basılırsa değişken aktif olup config gönderme işlemi yapıyor.
/*    if(stt==2){
      kk=0;
      enableInterrupt = false;
      Serial.println(" frq Datalar gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(send_frq_config, &data.counter, data.tr_array);
      floatAddByte(frq,&data.counter,data.tr_array);
      uint8tAddByte(dbm,&data.counter,data.tr_array);
       for(int p =0;p<7;p++){
        Serial.print(data.tr_array[p],HEX);
        Serial.print(",");
        
      }
      Serial.println("");
      Serial.print(dbm);
      Serial.print(frq);
   //   delay(100);
      transmissionState = radio.startTransmit(data.tr_array,7);
      transmitFlag = true;
      stt=0;
      //ok=1;
      enableInterrupt = true;

}*/

if(stt==3){
      kk=0;
      enableInterrupt = false;
      Serial.println(" prsht Datalar gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(send_prsht_config, &data.counter, data.tr_array);
     // floatAddByte(apogee,&data.counter,data.tr_array);
      floatAddByte(main,&data.counter,data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,6);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;
      prsht_snd_err=1;
      timeout=millis();
}

    if(stt==4){
      kk=0;
      enableInterrupt = false;
      Serial.println(" prhst Datalar istendi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_prsht_config, &data.counter, data.tr_array);
      prsht_rqst_err=1;
      timeout_prsht = millis();
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

   if(stt==5){
      kk=0;
      enableInterrupt = false;
      Serial.println(" 1. pyro ateşleme gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(pyro_drg_config, &data.counter, data.tr_array);
      //delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

   if(stt==6){
      kk=0;
      enableInterrupt = false;
      Serial.println(" 2. pyro ateşleme gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(pyro_main_config, &data.counter, data.tr_array);
    //  delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

   if(stt==7){
      kk=0;
      enableInterrupt = false;
      Serial.println(" FLASH SİLME İSTEĞİ GÖNDERİLDİ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(flash_erase, &data.counter, data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

   if(stt==8){
      kk=0;
      enableInterrupt = false;
      Serial.println(" yeni frekansta kontrol yapılıyor");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(check_frq, &data.counter, data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

//*********************************************************************************************************** 
/*
if(kk==0 && screen_id==2 && flash_succes==0 && wrng==1 && (i==16 || i==8) && cls==1){
  
    if(millis()-timeout_flash > 2000){
    
      ct++;
      if(ct==2){flash_error=1; ct=0;}
     
      timeout_flash = millis();
     
    }
  }*/

  if(kk==0 && flash_succes==0 && cls==1 ){
  
    if(millis()-timeout_flash > 2000){
    
      ct++;
      stt=7;
      if(ct==3 && cls==1){flash_error=1; ct=0; cls=0;}
     
      timeout_flash = millis();
     
    }
  }

//***********************************************************************************************************   
/*if(kk==0 && i==7 && time_com==0 && rqst_err==1){
  
    if(millis()-timeout > 1000){
     stt=1;
     timeout = millis();
     cnt+=1;
     Serial.print(" frekans veriler ");
     Serial.print(cnt);
     Serial.print(". kez istendi");
    }
  
  if(cnt>=5){cnt=0; time_com=1;rqst_err=0; v=2;}
  
  }*/

//***********************************************************************************************************  

 /* if(kk==0 && i==7 && time_com==0 && prsht_rqst_err==1){
  
    if(millis()-timeout > 1000){
     stt=4;
     timeout = millis();
     cnt+=1;
     Serial.print(" prst veriler ");
     Serial.print(cnt);
     Serial.print(". kez istendi");
    }
  
  if(cnt>=5){cnt=0; time_com=1;prsht_rqst_err=0; v=2;}
  
  }*/




/*if((kk==0 && i==7 && time_com==0 && rqst_err==1) || (kk==0 && i==7 && time_com==0 && prsht_rqst_err==1)){


  
  if(millis()-timeout > 1000){
     if(rqst_err==1)stt=1;
      else stt=4;
     timeout = millis();
     cnt+=1;
     Serial.print(" veriler ");
     Serial.print(cnt);
     Serial.print(". kez istendi");
    }

    if(cnt>=5) {
      
      time_com=1; v=2;     //time_com haberleşmede 5 defa denemeyi kesmek için kullanılan değişken.--- V ise ekrana sadece bir defa uyarı basmak için kullanılıyor.
      if(last_st==1 && new_frq==0){
      delay(2000);
      cnt=0;
      frq=last_frq;
      dbm=last_dbm;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      rqst_err=1;
      time_com=0;
      Serial.println("ESKİ FREKANSA GEÇİLDİ");
      last_st=0;
        }
      
      
      }
    
  }*/


//*********************************************************************************************************** 
  

/*if(kk==0 && i==7 && time_com==0 && snd_err==1){
  
  
   if(millis()-timeout > 500){
     stt=2;
     timeout = millis();
     cnt+=1;
     Serial.print(" frq veriler ");
     Serial.print(cnt);
     Serial.print(". kez gönderildi");
    }
  
    if(cnt>=3) {time_com=1; ok=1; cnt=0; } 
  
  }*/

//***********************************************************************************************************   
   if((kk==0 && prsht_snd_err==1)){

    if(millis()-timeout_prsht > 1000){
     stt=3;
     timeout_prsht = millis();
     cnt++;
     Serial.print(" prst veriler ");
     Serial.print(cnt);
     Serial.println(". kez gönderildi");
    }

    if(cnt>=5) {stt=0;prsht_snd_err=0;cnt=0;}     // haberleşmede 5 defa denemeyi kesmek için kullanılan değişken.
  }

//*********************************************************************************************************** 

  if( new_frq==0 && last_st==1 ){
    

   if(millis()-timeout > 1000){
     stt=8;
     timeout = millis();
     cnt+=1;
     Serial.print(" yeni frq kontrol ");
     Serial.print(cnt);
     Serial.print(". kez gönderildi");
    }
   if(cnt>=5 && new_frq==0){cnt=0;
      delay(2000);
      frq=last_frq;
      dbm=last_dbm;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      Serial.println("ESKİ FREKANSA GEÇİLDİ");
      last_st=0;
      v=10;
      
        }
    }
//***********************************************************************************************************    
    
    if(operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;

      if(transmitFlag) {


      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
       // Serial.println(F("transmission finished!"));
  
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
  
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;
     // Serial.println("bekliyor");
      
    }

    else{
   // Serial.println("alım başlayacak");
    int state = radio.readData(data.byteArray,150);
    
    if(state == ERR_CRC_MISMATCH){
      
      crc_err+=1;
           
      }
    



    if (state == ERR_NONE) {
    /*  for(int p=0;p<130;p++) {
            Serial.print(data.byteArray[p], HEX);
            Serial.print(",");
          }*/
      
      timeout_comm= millis();

      if(data.byteArray[0] == passw && data.byteArray[1] == getsensData ){
    bt_send=1;
    flight_mode=0;
    
data.counter=2;
for( ii=0;ii<3;ii++){ 
flightData.H3LIS331_data[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

     if(flightData.H3LIS331_data[ii]>flightstatMax.H3LIS331_data[ii])flightstatMax.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];
else if(flightData.H3LIS331_data[ii]>flightstatMin.H3LIS331_data[ii])flightstatMin.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];

}

flightData.Ms5611_data[0]= float(byteToInt16(data.byteArray, &data.counter))/100;

flightData.Ms5611_data[2]= byteToFloat(data.byteArray, &data.counter);

flightData.Ms5611_data[3]= byteToFloat(data.byteArray, &data.counter);


     if(flightData.Ms5611_data[2]>flightData.MAX_ALT)flightData.MAX_ALT=flightData.Ms5611_data[2];
else if(flightData.Ms5611_data[3]>flightData.MAX_SPEED)flightData.MAX_SPEED=flightData.Ms5611_data[3];

for( ii=0;ii<3;ii++){ 
flightData.bnoAccel[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

if(abs(flightData.bnoAccel[ii])>abs(flightData.MAX_ACCEL[ii]))flightData.MAX_ACCEL[ii]=flightData.bnoAccel[ii];

}



for( ii=0;ii<3;ii++){ 
flightData.bnoGyro[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}
for( ii=0;ii<3;ii++){ 
flightData.bnoMag[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}
for( ii=0;ii<3;ii++){ 
flightData.bnoRaw[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}

for( ii=0;ii<3;ii++){ 
flightData.GPS_data[ii]= byteToLong(data.byteArray, &data.counter);

}
flightData.GPS_speed=float(byteToInt16(data.byteArray, &data.counter))/100;



flightData.unix_time= byteToLong(data.byteArray, &data.counter);



flightData.batteryy= float(byteToInt16(data.byteArray, &data.counter))/100;


flightData.SIV=byteToUint8t(data.byteArray, &data.counter);


pyro_stat = byteToUint8t(data.byteArray, &data.counter);
flightData.mach_lock = byteToUint8t(data.byteArray, &data.counter);
Serial.print(" MACHLOCK ");
Serial.println(flightData.mach_lock);
//Serial.print(pyro_stat);
/*for (int i = 0;i<8; i++) {
        pyro_stat_arr[i] = 0;   
    }
for (int i = 0; pyro_stat > 0; i++) {
        pyro_stat_arr[i] = pyro_stat % 2;
        pyro_stat = pyro_stat / 2;   
    }*/


flightData.rocket_flash= float(byteToInt16(data.byteArray, &data.counter))/100;
   


FLIGHT_TIME = (flightData.APOGEE_TIME/1000) + (flightData.DESCENT_TIME/1000);


    All_data1=String(flightData.H3LIS331_data[0])+","+String(flightData.H3LIS331_data[1])+","+String(flightData.H3LIS331_data[2])+",";
    All_data1+=String(flightData.Ms5611_data[0])+","+String(flightData.Ms5611_data[2])+","+String(flightData.Ms5611_data[3])+",";
    All_data1+=String(flightData.bnoAccel[0])+","+String(flightData.bnoAccel[1])+","+String(flightData.bnoAccel[2])+",";
    All_data1+=String(flightData.bnoGyro[0])+","+String(flightData.bnoGyro[1])+","+String(flightData.bnoGyro[2])+",";
    All_data1+=String(flightData.bnoMag[0])+","+String(flightData.bnoMag[1])+","+String(flightData.bnoMag[2])+",";
    All_data1+=String(flightData.bnoRaw[0])+","+String(flightData.bnoRaw[1])+","+String(flightData.bnoRaw[2])+",";
    All_data1+=String(flightData.GPS_data[0])+","+String(flightData.GPS_data[1])+","+String(flightData.GPS_data[2])+","+String(flightData.GPS_speed)+",";
    All_data1+=String(flightData.SIV)+","+String(flightData.batteryy)+","+String(pyro1)+","+String(pyro2)+",";
    All_data1+=String(flightData.MAX_ALT)+","+String(flightData.MAX_SPEED)+","+String(flightData.MAX_ACCEL[0])+","+String(flightData.MAX_ACCEL[1])+","+String(flightData.MAX_ACCEL[2])+",";
    All_data1+=String(flightData.RISE_ACC[0])+","+String(flightData.RISE_ACC[1])+","+String(flightData.RISE_ACC[2])+","+String(flightData.FALL_ACC[0])+","+String(flightData.FALL_ACC[1])+","+String(flightData.FALL_ACC[2])+",";
    All_data1+=String(flightData.MAX_SIV)+","+String(flightData.ENGINEBURN_TIME)+","+String(flightData.pyro_burn_time)+","+String(flightData.APOGEE_TIME)+","+String(flightData.APOGEE_FALL_SPEED)+",";
    All_data1+=String(flightData.MAIN_FALL_SPEED)+","+String(flightData.DESCENT_TIME)+","+String(FLIGHT_TIME)+","+String(flightData.unix_time)+"\n";
    //   uint32_t t = millis(); 
   // Serial.println(All_data1);
  
EEPROM.writeFloat(42,flightData.GPS_data[0]);
EEPROM.writeFloat(46,flightData.GPS_data[1]); 
 All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 char chrr_header[100]; 
 All_data1.toCharArray(chrr, All_dataa);
 header.toCharArray(chrr_header, All_dataa);
 if(header_count==1){
  appendFile(SD, "/GROUNDSTATION.csv", header);
  header_count=0;
  }
 appendFile(SD, "/GROUNDSTATION.csv",  All_data1);
 All_data1="";
       
        }
     
      if( data.byteArray[0] == passw && data.byteArray[1] == getflightData ){ 
      flight_mode=1;
      bt_send_flight=1;
       Serial.print("FLIGHT MODE ");
       Serial.println(flight_mode);
data.counter=2;
for( ii=0;ii<3;ii++){ 
flightData.H3LIS331_data[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

     if(flightData.H3LIS331_data[ii]>flightstatMax.H3LIS331_data[ii])flightstatMax.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];
else if(flightData.H3LIS331_data[ii]>flightstatMin.H3LIS331_data[ii])flightstatMin.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];

}

flightData.Ms5611_data[0]= float(byteToInt16(data.byteArray, &data.counter))/100;

flightData.Ms5611_data[2]= byteToFloat(data.byteArray, &data.counter);

flightData.Ms5611_data[3]= byteToFloat(data.byteArray, &data.counter);


     if(flightData.Ms5611_data[2]>flightData.MAX_ALT)flightData.MAX_ALT=flightData.Ms5611_data[2];
else if(flightData.Ms5611_data[3]>flightData.MAX_SPEED)flightData.MAX_SPEED=flightData.Ms5611_data[3];

for( ii=0;ii<3;ii++){ 
flightData.bnoAccel[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

 if(abs(flightData.bnoAccel[ii])>abs(flightData.MAX_ACCEL[ii]))flightData.MAX_ACCEL[ii]=flightData.bnoAccel[ii];

}



for( ii=0;ii<3;ii++){ 
flightData.bnoGyro[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}
for( ii=0;ii<3;ii++){ 
flightData.bnoMag[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}
for( ii=0;ii<3;ii++){ 
flightData.bnoRaw[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}

for( ii=0;ii<3;ii++){ 
flightData.GPS_data[ii]= byteToLong(data.byteArray, &data.counter);

}
flightData.GPS_speed=float(byteToInt16(data.byteArray, &data.counter))/100;



flightData.unix_time= byteToLong(data.byteArray, &data.counter);



flightData.batteryy= float(byteToInt16(data.byteArray, &data.counter))/100;


flightData.SIV= byteToUint8t(data.byteArray, &data.counter);


pyro_stat =byteToUint8t(data.byteArray, &data.counter);
flightData.mach_lock = byteToUint8t(data.byteArray, &data.counter);
//Serial.print(pyro_stat);
/*for (int i = 0;i<8; i++) {
        pyro_stat_arr[i] = 0;   
    }
for (int i = 0; pyro_stat > 0; i++) {
        pyro_stat_arr[i] = pyro_stat % 2;
        pyro_stat = pyro_stat / 2;   
    }*/
    
    





       All_data1=String(flightData.H3LIS331_data[0])+","+String(flightData.H3LIS331_data[1])+","+String(flightData.H3LIS331_data[2])+",";
    All_data1+=String(flightData.Ms5611_data[0])+","+String(flightData.Ms5611_data[2])+","+String(flightData.Ms5611_data[3])+",";
    All_data1+=String(flightData.bnoAccel[0])+","+String(flightData.bnoAccel[1])+","+String(flightData.bnoAccel[2])+",";
    All_data1+=String(flightData.bnoGyro[0])+","+String(flightData.bnoGyro[1])+","+String(flightData.bnoGyro[2])+",";
    All_data1+=String(flightData.bnoMag[0])+","+String(flightData.bnoMag[1])+","+String(flightData.bnoMag[2])+",";
    All_data1+=String(flightData.bnoRaw[0])+","+String(flightData.bnoRaw[1])+","+String(flightData.bnoRaw[2])+",";
    All_data1+=String(flightData.GPS_data[0])+","+String(flightData.GPS_data[1])+","+String(flightData.GPS_data[2])+","+String(flightData.GPS_speed)+",";
    All_data1+=String(flightData.SIV)+","+String(flightData.batteryy)+","+String(pyro1)+","+String(pyro2)+",";
    All_data1+=String(flightData.MAX_ALT)+","+String(flightData.MAX_SPEED)+","+String(flightData.MAX_ACCEL[0])+","+String(flightData.MAX_ACCEL[1])+","+String(flightData.MAX_ACCEL[2])+",";
    All_data1+=String(flightData.RISE_ACC[0])+","+String(flightData.RISE_ACC[1])+","+String(flightData.RISE_ACC[2])+","+String(flightData.FALL_ACC[0])+","+String(flightData.FALL_ACC[1])+","+String(flightData.FALL_ACC[2])+",";
    All_data1+=String(flightData.MAX_SIV)+","+String(flightData.ENGINEBURN_TIME)+","+String(flightData.pyro_burn_time)+","+String(flightData.APOGEE_TIME)+","+String(flightData.APOGEE_FALL_SPEED)+",";
    All_data1+=String(flightData.MAIN_FALL_SPEED)+","+String(flightData.DESCENT_TIME)+","+String(FLIGHT_TIME)+","+String(flightData.unix_time)+"\n";
    
EEPROM.writeFloat(42,flightData.GPS_data[0]);
EEPROM.writeFloat(46,flightData.GPS_data[1]);
  //   uint32_t t = millis(); 
       
 All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 char chrr_header[All_dataa]; 
 All_data1.toCharArray(chrr, All_dataa);
 header.toCharArray(chrr_header, All_dataa);
 if(header_count==1){
  appendFile(SD, "/GROUNDSTATION.csv", header);
  header_count=0;
  }
 appendFile(SD, "/GROUNDSTATION.csv",  All_data1);
 All_data1="";
 
    }

 if(data.byteArray[0] == passw && data.byteArray[1] == getsensData_flgtstcs  ){
  bt_send=1;
  bt_send_flight=0;
  bt_send_sttcs=1;
  flight_mode=1;
   Serial.print("FLIGHT MODE STTCS ");
   Serial.println(bt_send_sttcs);
data.counter=2;
for( ii=0;ii<3;ii++){ 
flightData.H3LIS331_data[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}

flightData.Ms5611_data[0]= float(byteToInt16(data.byteArray, &data.counter))/100;

flightData.Ms5611_data[2]= byteToFloat(data.byteArray, &data.counter);

flightData.Ms5611_data[3]= byteToFloat(data.byteArray, &data.counter);



for( ii=0;ii<3;ii++){ 
flightData.bnoAccel[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}

for( ii=0;ii<3;ii++){ 
flightData.bnoGyro[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}
for( ii=0;ii<3;ii++){ 
flightData.bnoMag[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}
for( ii=0;ii<3;ii++){ 
flightData.bnoRaw[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}

for( ii=0;ii<3;ii++){ 
flightData.GPS_data[ii]=byteToLong(data.byteArray, &data.counter);

}
flightData.GPS_speed=float(byteToInt16(data.byteArray, &data.counter))/100;


flightData.unix_time= byteToLong(data.byteArray, &data.counter);



flightData.batteryy= float(byteToInt16(data.byteArray, &data.counter))/100;


flightData.SIV= byteToUint8t(data.byteArray, &data.counter);


pyro_stat =byteToUint8t(data.byteArray, &data.counter);
flightData.mach_lock = byteToUint8t(data.byteArray, &data.counter);
//Serial.print(pyro_stat);
/*for (int i = 0;i<8; i++) {
        pyro_stat_arr[i] = 0;   
    }
for (int i = 0; pyro_stat > 0; i++) {
        pyro_stat_arr[i] = pyro_stat % 2;
        pyro_stat = pyro_stat / 2;   
    }*/
    


flightData.MAX_ALT= byteToFloat(data.byteArray, &data.counter);



flightData.MAX_SPEED= float(byteToInt16(data.byteArray, &data.counter))/50;


for( ii=0;ii<3;ii++){ 
flightData.MAX_ACCEL[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

}

flightData.MAX_SIV= byteToUint8t(data.byteArray, &data.counter);


flightData.ENGINEBURN_TIME = byteToByte(data.byteArray, &data.counter);


flightData.APOGEE_TIME= byteToUint16(data.byteArray, &data.counter);



flightData.APOGEE_FALL_SPEED =byteToFloat(data.byteArray, &data.counter);


flightData.MAIN_FALL_SPEED = byteToFloat(data.byteArray, &data.counter);


flightData.DESCENT_TIME= byteToUint16(data.byteArray, &data.counter);


flightData.pyro_burn_time= byteToLong(data.byteArray, &data.counter);


// BU AMK DATALARINI FOR A SOKMA TEK TEK PARÇALA //

flightData.RISE_ACC[0]=  byteToFloat(data.byteArray, &data.counter);
flightData.RISE_ACC[1]=  byteToFloat(data.byteArray, &data.counter);
flightData.RISE_ACC[2]=  byteToFloat(data.byteArray, &data.counter);

flightData.FALL_ACC[0]=  byteToFloat(data.byteArray, &data.counter);
flightData.FALL_ACC[1]=  byteToFloat(data.byteArray, &data.counter);
flightData.FALL_ACC[2]=  byteToFloat(data.byteArray, &data.counter);





FLIGHT_TIME = (flightData.APOGEE_TIME) + (flightData.DESCENT_TIME);



    All_data1=String(flightData.H3LIS331_data[0])+","+String(flightData.H3LIS331_data[1])+","+String(flightData.H3LIS331_data[2])+",";
    All_data1+=String(flightData.Ms5611_data[0])+","+String(flightData.Ms5611_data[2])+","+String(flightData.Ms5611_data[3])+",";
    All_data1+=String(flightData.bnoAccel[0])+","+String(flightData.bnoAccel[1])+","+String(flightData.bnoAccel[2])+",";
    All_data1+=String(flightData.bnoGyro[0])+","+String(flightData.bnoGyro[1])+","+String(flightData.bnoGyro[2])+",";
    All_data1+=String(flightData.bnoMag[0])+","+String(flightData.bnoMag[1])+","+String(flightData.bnoMag[2])+",";
    All_data1+=String(flightData.bnoRaw[0])+","+String(flightData.bnoRaw[1])+","+String(flightData.bnoRaw[2])+",";
    All_data1+=String(flightData.GPS_data[0])+","+String(flightData.GPS_data[1])+","+String(flightData.GPS_data[2])+","+String(flightData.GPS_speed)+",";
    All_data1+=String(flightData.SIV)+","+String(flightData.batteryy)+","+String(pyro1)+","+String(pyro2)+",";
    All_data1+=String(flightData.MAX_ALT)+","+String(flightData.MAX_SPEED)+","+String(flightData.MAX_ACCEL[0])+","+String(flightData.MAX_ACCEL[1])+","+String(flightData.MAX_ACCEL[2])+",";
    All_data1+=String(flightData.RISE_ACC[0])+","+String(flightData.RISE_ACC[1])+","+String(flightData.RISE_ACC[2])+","+String(flightData.FALL_ACC[0])+","+String(flightData.FALL_ACC[1])+","+String(flightData.FALL_ACC[2])+",";
    All_data1+=String(flightData.MAX_SIV)+","+String(flightData.ENGINEBURN_TIME)+","+String(flightData.pyro_burn_time)+","+String(flightData.APOGEE_TIME)+","+String(flightData.APOGEE_FALL_SPEED)+",";
    All_data1+=String(flightData.MAIN_FALL_SPEED)+","+String(flightData.DESCENT_TIME)+","+String(FLIGHT_TIME)+","+String(flightData.unix_time)+"\n";

EEPROM.writeFloat(42,flightData.GPS_data[0]);
EEPROM.writeFloat(46,flightData.GPS_data[1]);
  //   uint32_t t = millis(); 
       
 All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 char chrr_header[All_dataa]; 
 All_data1.toCharArray(chrr, All_dataa);
 header.toCharArray(chrr_header, All_dataa);
 if(header_count==1){
  appendFile(SD, "/GROUNDSTATION.csv", header);
  header_count=0;
  }
 appendFile(SD, "/GROUNDSTATION.csv",  All_data1);
 All_data1="";
 
    }



if(data.byteArray[0] == passw && (data.byteArray[1] == getsensData_tch_grnd) || data.byteArray[1] == getsensData_tch_grnd_all){

bt_send_data_after_flight=1;
flight_mode=2;

  data.counter=2;
  for( ii=0;ii<3;ii++){ 
  flightData.GPS_data[ii]= byteToLong(data.byteArray, &data.counter);
  }
 flightData.SIV= byteToByte(data.byteArray, &data.counter);
 flightData.batteryy= byteToFloat(data.byteArray, &data.counter);
  
 

if(data.byteArray[1] == getsensData_tch_grnd_all)
{
  
  flightData.MAX_ALT= byteToFloat(data.byteArray, &data.counter);

  flightData.MAX_SPEED= float(byteToInt16(data.byteArray, &data.counter))/50;


  for( ii=0;ii<3;ii++){ 
    flightData.MAX_ACCEL[ii]= float(byteToInt16(data.byteArray, &data.counter))/100;

   }

  flightData.MAX_SIV= byteToByte(data.byteArray, &data.counter);


  flightData.ENGINEBURN_TIME = byteToByte(data.byteArray, &data.counter);


  flightData.APOGEE_TIME= byteToUint16(data.byteArray, &data.counter);


  flightData.APOGEE_FALL_SPEED = byteToFloat(data.byteArray, &data.counter);


  flightData.MAIN_FALL_SPEED = byteToFloat(data.byteArray, &data.counter);


  flightData.DESCENT_TIME= byteToUint16(data.byteArray, &data.counter);

 flightData.pyro_burn_time= byteToLong(data.byteArray, &data.counter);
 
flightData.RISE_ACC[0]=  byteToFloat(data.byteArray, &data.counter);
flightData.RISE_ACC[1]=  byteToFloat(data.byteArray, &data.counter);
flightData.RISE_ACC[2]=  byteToFloat(data.byteArray, &data.counter);

flightData.FALL_ACC[0]=  byteToFloat(data.byteArray, &data.counter);
flightData.FALL_ACC[1]=  byteToFloat(data.byteArray, &data.counter);
flightData.FALL_ACC[2]=  byteToFloat(data.byteArray, &data.counter);


  
  
flightData.rocket_flash= float(byteToInt16(data.byteArray, &data.counter))/100;

  FLIGHT_TIME = (flightData.APOGEE_TIME) + (flightData.DESCENT_TIME);


  
  }
EEPROM.writeFloat(42,flightData.GPS_data[0]);
EEPROM.writeFloat(46,flightData.GPS_data[1]);
}


    if(data.byteArray[0] == passw && data.byteArray[1] == get_frq_config){
      
      data.counter=2;
      carrierFreq = byteToFloat(data.byteArray, &data.counter);
      outputPower = byteToUint8t(data.byteArray, &data.counter);
      kk=1;                                                // config alındığına dair bilgi için değişken.
      cnt=0;
      get_frq_data=1;
      let_send=1;
        frq=carrierFreq;
        dbm=outputPower;
        last_frq=frq;  
        last_dbm=dbm;
      //Serial.print(" ilk config alındı ");
        //if(frq==carrierFreq && dbm==outputPower && frst_cnt > 1){ compare=1; Serial.print(" karşılaştırma başarılı ");   }   // data karşılaştırma
        //if(frst_cnt==1){ compare=4;frst_cnt=0; Serial.print(" ilk config alındı ");}
        //else if(!(frq==carrierFreq && dbm==outputPower )){compare=0; v=4; Serial.print(" karşılaştırma başşarısız ");}
      

     //  ilk data istemede gelen datalar istasyondaki değişkenlere atanıyor ekranda görebilmek için
     
     // if(check_data==0 && !(compare==0)){
        

      //  check_data=1;                // sadece bir defa eşitleme yapması için kullanılıyor. başarısız haberleşme sonucunda tekrar istemede 0 yapılacak
   
            }

         else if(data.byteArray[0] == passw && data.byteArray[1] == check_frq){
            Serial.println(" yeni frekans teyit edildi ");
            new_frq=1;
            v=8;
            last_frq=frq;  
            last_dbm=dbm;
             Serial.print(" new_frq= "); 
             Serial.println( new_frq); 
            } 
          

     else if(data.byteArray[0] == passw && data.byteArray[1] == get_prsht_config){
      Serial.print(" paraşüt geri geldi ");
      data.counter=2;
      rocketmain = byteToFloat(data.byteArray, &data.counter);
      kk=1;                         // config alındığına dair bilgi için değişken.
      cnt=0;                        // tekrar göndermedeki sayacı sıfırlamak için değişken
      //frst_cnt+=1;                  // ilk alımda karşılaştırma yapmaması için

        
        if( main==rocketmain && prsht_snd_err == 1){ prsht_succes=1; compare=1;prsht_rqst_err=0;Serial.print(" paraşüt karşılaştırma başarılı ");   }   // data karşılaştırma
        else if(prsht_rqst_err==1){ compare=4; prsht_rqst_err=0;  Serial.print(" paraşüt ilk config alındı ");}
        else if(!(main==rocketmain)){compare=0; v=4;prsht_rqst_err=0;prsht_snd_err =0; Serial.print(" paraşüt karşılaştırma başşarısız ");}
      

     //  ilk data istemede gelen datalar istasyondaki değişkenlere atanıyor ekranda görebilmek için
     
      if(!(compare==0)){
        
        main=rocketmain;
        prsht_snd_err =0;           
        bl_rqst=1;
            }
            }   
          

          else if(data.byteArray[0] == passw && data.byteArray[1] == pyro_drg_config){
            
            pyro_succes=1;
            
            
            }

             else if(data.byteArray[0] == passw && data.byteArray[1] == send_prsht_config){
            
            
            compare=1;
            
            }

             else if(data.byteArray[0] == passw && data.byteArray[1] == flash_erase){

            data.counter=2;
            flightData.rocket_flash= float(byteToInt16(data.byteArray, &data.counter))/100;
            flash_succes=1;
            Serial.println("flash doluluk ");
            Serial.println(flightData.rocket_flash);

            }

        
       
    }
    }

    radio.startReceive();
    enableInterrupt = true;

 }

//*******************************************************************************
 if(kk==0 && prsht_rqst_err==1){
  
    if(millis()-timeout > 1000){
     stt=4;
     timeout = millis();
     cnt++;
     Serial.print(" prst veriler ");
     Serial.print(cnt);
     Serial.print(". kez istendi");
    }
  
  if(cnt>=5){cnt=0; prsht_rqst_err=0;stt=0;}
  
  }

//*******************************************************************************
 if(new_frq==1){
  
      EEPROM.writeFloat(0,frq);
      EEPROM.writeInt(6,dbm);
      EEPROM.commit();
  
  }

if(gps_cnt==0 && tst==1){
  
 All_data2=String(buf)+","+String(time_gpsfix/1000)+"\n";
appendFile(SD, "/gpstime.csv",  All_data2);
 All_data2="";
 tst=0;
}

 if((millis()-timeout_comm > 25000 && (flight_mode==1 || flight_mode==2)) || flight_mode==2 ){
tch_grnd=1;
timeout_comm = millis();
}

if(tch_grnd==1){
  if(millis()-timeout_fall > 2000){
    
      kk=0;
      enableInterrupt = false;
      
      
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      if( flightData.DESCENT_TIME > 1 ){
        Serial.println("  SADECE GPS DATA İSTENDİ ");
        byteAddByte(getsensData_tch_grnd, &data.counter, data.tr_array);
      }
      else {
        Serial.println("  istatistikli GPS DATA İSTENDİ ");
        byteAddByte(getsensData_tch_grnd_all, &data.counter, data.tr_array); }
     // delay(1000);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      enableInterrupt = true;
      timeout_fall = millis();

}
}



if( ok==1){


      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      Serial.println(" frekans değişti ");
      Serial.println(" birazdan yeni frekansta istek yapılacak.");
      delay(1000);
      
      /*enableInterrupt = false;
      data.counter=0;
      Serial.print(" güncelleme bilgisi gönderildi ");
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(setConfig, &data.counter, data.tr_array);
      delay(200);
      transmissionState = radio.startTransmit(data.tr_array,2);
      
       for(int p =0;p<2;p++){
        Serial.print(data.tr_array[p],HEX);
        Serial.print(",");
        
      }
      Serial.println("");
      
      transmitFlag = true;*/

      EEPROM.writeFloat(0,frq);
      EEPROM.writeInt(6,dbm);
      EEPROM.commit();
      Serial.print(" HAFIZAYA ALINDI ");
      stt=8;
      ok=0;
      last_st=1;
  
    


  }

  if(selffrq_set==1){
  
  radio.setFrequency(frq);
  radio.setOutputPower(dbm);
  EEPROM.writeFloat(0,frq);
  EEPROM.writeInt(6,dbm);
  EEPROM.commit();
  selffrq_set=0;
  }
/*
Serial.print(" V= ");
Serial.println(v);

Serial.print(" try_com= ");
Serial.println(try_com);

Serial.print(" compare= ");
Serial.println(compare);

Serial.print(" time_com= ");
Serial.println(time_com);*/
/*timeout_hz=millis()-timeout_hz;

if(timeout_hz >= 100){
Serial.print(" 0. çekirdek süre ");
Serial.println(timeout_hz);}*/

}
 
}


double ReadVoltage(byte pin){
  double reading = analogRead(pin); // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
  if(reading < 1 || reading > 4095) return 0;
  // return -0.000000000009824 * pow(reading,3) + 0.000000016557283 * pow(reading,2) + 0.000854596860691 * reading + 0.065440348345433;
  return -0.000000000000016 * pow(reading,4) + 0.000000000118171 * pow(reading,3)- 0.000000301211691 * pow(reading,2)+ 0.001109019271794 * reading + 0.034143524634089;
}


  void loop() {



      if (SerialBT.available()) {
         doc.clear(); 
         DeserializationError error = deserializeJson(doc, SerialBT);
         serializeJsonPretty(doc, Serial);
        if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
            return;
     }
    
     
    if(!(doc["MAIN"]) && !(doc["APOGEE"]) &&!(doc["CONFIG"]["RQ"]==true) &&!(doc["CONFIG"]["FRQ"]==frq))         { frq = doc["CONFIG"]["FRQ"];selffrq_set=1; Serial.println("FREKANS DEGISIK GELDI"); }
    if(!(doc["MAIN"]) && !(doc["APOGEE"]) &&!(doc["CONFIG"]["RQ"]==true) &&!(doc["CONFIG"]["DBM"]==dbm))         { dbm = doc["CONFIG"]["DBM"];selffrq_set=1; Serial.println("DBM DEGISIK GELDI"); }
    if(!(doc["MAIN"]) && !(doc["APOGEE"]) &&!(doc["CONFIG"]["RQ"]==true) &&!(doc["CONFIG"]["PRSHT"]==main))      { main= doc["CONFIG"]["PRSHT"]; stt=3;      Serial.println("PRSHT DEPISIK GELDI");}
    if(!(doc["MAIN"]) && !(doc["APOGEE"]) &&!(doc["CONFIG"]["RQ"]==true) && (doc["CONFIG"]["FLSH_CLEAN"]==true)) { stt=7; cls=1; timeout_flash = millis(); Serial.println("FLASH TEMIZLEME GELDI");}
    if(!(doc["MAIN"]) && !(doc["APOGEE"]) &&!(doc["CONFIG"]["RQ"]==true) && (doc["CONFIG"]["UNIT"]=="IMPERIAL")) { u=0;Serial.println("IMPERIAL GELDI");}else{u=1;}
    if(!(doc["MAIN"]) && !(doc["APOGEE"]) &&  doc["CONFIG"]["RQ"]==true)                                         { stt=4; Serial.println("CONFIG ISTENDI");}else{snd_status=1;}


    if((doc["APOGEE"])==1)stt=5;
    delay(500);
    if((doc["MAIN"])==1 )stt=6;
    doc.clear();
  }


//if(snd_status==1){

  if(prsht_succes==1){
     doc.clear();
     s1.getBytes(btarray, 2);
     SerialBT.write(btarray[0]);
     delay(10);
     doc["CONFIG"]["STATUS"] = 1;
     if(u==1)doc["CONFIG"]["UNIT"] = "METRIC";
     else    doc["CONFIG"]["UNIT"] = "IMPERIAL";
     serializeJson(doc, SerialBT);
     serializeJsonPretty(doc, Serial);
     Serial.println("onay gönderildi");
     delay(30);
     SerialBT.write(btarray[0]);
     delay(10);
   // snd_status=0;
     prsht_succes=0;
  }

   else if(compare==0 && prsht_succes==0){
  
     doc.clear();
     s1.getBytes(btarray, 2);
     SerialBT.write(btarray[0]);
     delay(10);
     doc["CONFIG"]["STATUS"] = 0;
     if(u==1)doc["CONFIG"]["UNIT"] = "METRIC";
     else    doc["CONFIG"]["UNIT"] = "IMPERIAL";
     serializeJson(doc, SerialBT);
     serializeJsonPretty(doc, Serial);
     Serial.println(" paraşüt onaylanamadı ");
     delay(30);
     SerialBT.write(btarray[0]);
     delay(10);
     compare=2;
  // snd_status=0;
   }

   
  
 // }

  if(flash_succes==1 && cls==1){
    
     doc.clear();
     s1.getBytes(btarray, 2);
     SerialBT.write(btarray[0]);
     delay(10);
     doc["CONFIG"]["STATUS"] = 1;
     serializeJson(doc, SerialBT);
     serializeJsonPretty(doc, Serial);
     Serial.println(" flash onaylandı ");
     delay(30);
     SerialBT.write(btarray[0]);
     delay(10);
    flash_succes=0;
    cls=0;
    ct=0;
    }
    else if(flash_error==1){
      
     doc.clear();
     s1.getBytes(btarray, 2);
     SerialBT.write(btarray[0]);
     delay(10);
     doc["CONFIG"]["STATUS"] = 0;  
     serializeJson(doc, SerialBT);
     serializeJsonPretty(doc, Serial);
     Serial.println(" flash !!!! onaylanmadı !!!! ");
     delay(30);
     SerialBT.write(btarray[0]);
     delay(10);
     flash_error=0;
      }

    
  
  if(bl_rqst==1){
     doc.clear();
     s1.getBytes(btarray, 2);
     SerialBT.write(btarray[0]);
     delay(10);
     doc["CONFIG"]["FRQ"] = frq;
     doc["CONFIG"]["DBM"] = dbm;
     doc["CONFIG"]["PRSHT"] = main;
     doc["CONFIG"]["STATUS"] = 2;
     if(u==1)doc["CONFIG"]["UNIT"] = "METRIC";
     else    doc["CONFIG"]["UNIT"] = "IMPERIAL";
     serializeJson(doc, SerialBT);
     serializeJsonPretty(doc, Serial);
     //Serial.println("CONFIG ISTEĞI GONDERILDI");
     delay(30);
     SerialBT.write(btarray[0]);
     delay(10);
     bl_rqst=0;

    }

switch(pyro_stat) {
          
             case 0:{
            pyro1=0;
            pyro2=0;}
            break;

             case 1:{
            pyro1=1;
            pyro2=0;}
            break;
            
             case 2:{
            pyro1=0;
            pyro2=1;}
            break;
            
             case 3:{
            pyro1=1;
            pyro2=1;}
            break;
            
             case 4:{
            pyro1=2;
            pyro2=0;}
            break;
            
             case 5:{
            pyro1=3;
            pyro2=0; }  
            break;
            
            case 6:{
           pyro1=2;
           pyro2=1;}
           break;
            
            case 7 :{
           pyro1=3;
           pyro2=1;}    
           break;
            
            case 8 :{
           pyro1=0;
           pyro2=2; }  
           break;
            
            case 9 :{
           pyro1=1;
           pyro2=2; }
           break;
            
            case 10 :{
           pyro1=0;
           pyro2=3;}
           break;
            
            case 11 :{
           pyro1=1;
           pyro2=3; }
           break;
            
            case 12 :{
           pyro1=2;
           pyro2=2;  }
           break;
            
            case 13 :{
           pyro1=3;
           pyro2=2;}
           break;
            
            case 14 :{
           pyro1=2;
           pyro2=3; }
           break;

            case 15 :{
           pyro1=3;
           pyro2=3; }
           break;
          
    }
   
/*Serial.println("********************************************* ");
    Serial.print("PYRO 1 ");
    Serial.println(pyro1);
    Serial.print("PYRO 2 ");
    Serial.println(pyro2);

    delay(1000);*/
/*
 if(pyro_stat_arr[0]==0)pyro1=0;
 else pyro1=1;

 if(pyro_stat_arr[1]==0)pyro2=0;
 else pyro2=1;
 
 if(pyro_stat_arr[2]==1 && pyro_stat_arr[0]==0)pyro1=2;
 if(pyro_stat_arr[3]==1 && pyro_stat_arr[1]==0)pyro2=2;*/

    
/*  if(bt_send_sttcs==0){
  
 //sttcs.remove("CRC_ERR");
  doc.clear();
  
  
  }*/

  
 if((bt_send_sttcs==1 || bt_send==1 || bt_send_flight==1) && tch_grnd==0){

 
  
 bt_send=0;
 s1.getBytes(btarray, 2);
 SerialBT.write(btarray[0]);
 delay(20);
 doc.clear();
 doc["ROCKET"]["H3LIS331"]["ACCEL_X"]=flightData.H3LIS331_data[0];
 doc["ROCKET"]["H3LIS331"]["ACCEL_Y"]=flightData.H3LIS331_data[1];
 doc["ROCKET"]["H3LIS331"]["ACCEL_Z"]=flightData.H3LIS331_data[2];

 doc["ROCKET"]["MS5611"]["ALTITUDE"]=flightData.Ms5611_data[2];
 doc["ROCKET"]["MS5611"]["TEMP"]=flightData.Ms5611_data[0];
 doc["ROCKET"]["MS5611"]["SPEED"]=flightData.Ms5611_data[3];

   doc["ROCKET"]["FLIGHT"]["ID"]=4;
   doc["ROCKET"]["FLIGHT"]["MODE"]=flight_mode;
 double battt=ReadVoltage(35)*6.6;
 doc["ROCKET"]["BNO055"]["ACCEL_X"]=flightData.bnoAccel[0];
 doc["ROCKET"]["BNO055"]["ACCEL_Y"]=flightData.bnoAccel[1];
 doc["ROCKET"]["BNO055"]["ACCEL_Z"]=flightData.bnoAccel[2];

 doc["ROCKET"]["BNO055"]["GYRO_X"]=flightData.bnoGyro[0];
 doc["ROCKET"]["BNO055"]["GYRO_Y"]=flightData.bnoGyro[1];
 doc["ROCKET"]["BNO055"]["GYRO_Z"]=flightData.bnoGyro[2];

 doc["ROCKET"]["BNO055"]["MAG_X"]=flightData.bnoMag[0];
 doc["ROCKET"]["BNO055"]["MAG_Y"]=flightData.bnoMag[1];
 doc["ROCKET"]["BNO055"]["MAG_Z"]=flightData.bnoMag[2];

 doc["ROCKET"]["BNO055"]["YAW"]=flightData.bnoRaw[0];
 doc["ROCKET"]["BNO055"]["PITCH"]=flightData.bnoRaw[1];
 doc["ROCKET"]["BNO055"]["ROLL"]=flightData.bnoRaw[2];

// 40.081737914676076, 29.5215763658782
//40.08163292506371, 29.519985885472973
 doc["ROCKET"]["GPS"]["LAT"]=flightData.GPS_data[0];
 doc["ROCKET"]["GPS"]["LONG"]=flightData.GPS_data[1];
 doc["ROCKET"]["GPS"]["SAT"]=flightData.SIV;
 doc["ROCKET"]["GPS"]["SPEED"]=flightData.GPS_speed;

 doc["ROCKET"]["BATTERY"]["ROCKET"] = flightData.batteryy;

 //Serial.println("batarya ");
 //Serial.println(battt);
 
 doc["ROCKET"]["BATTERY"]["GS"] = battt;

 doc["ROCKET"]["SD_FREE"] = sd_cap;

 doc["ROCKET"]["LORA"]["RSSI"] = radio.getRSSI();
 
 doc["ROCKET"]["PYRO"]["APOGEE"]=pyro1;
 doc["ROCKET"]["PYRO"]["MAIN"]=pyro2;
 serializeJson(doc, SerialBT);
 delay(60);
 //Serial.println("ROCKET DATA GONDERILDI");
   }
 
 
 if(tch_grnd==1){

if(millis()-timeout_bluetooth >2000){
 
 bt_send_sttcs=0;
 s1.getBytes(btarray, 2);
 SerialBT.write(btarray[0]);
 delay(20);
 doc.clear();
 Serial.println("ISTATISTIC ");

 doc["FLIGHT_STATISTICS"]["FLIGHT"]["MODE"]=flight_mode;
 doc["FLIGHT_STATISTICS"]["GPS"]["LAT"]=flightData.GPS_data[0]; // HARİTA ROKET ARAMA SAYFASI KONUM İÇİN DATA
 doc["FLIGHT_STATISTICS"]["GPS"]["LONG"]=flightData.GPS_data[1]; //HARİTA ROKET ARAMA SAYFASI KONUM İÇİN DATA
 
 doc["FLIGHT_STATISTICS"]["CRC_ERR"] = crc_err;// istatistğe geç

 doc["FLIGHT_STATISTICS"]["MAX_ALTITUDE"]=flightData.MAX_ALT;
 doc["FLIGHT_STATISTICS"]["MAX_SPEED"]=flightData.MAX_SPEED;

 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_X"]=flightData.MAX_ACCEL[0];
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_Y"]=flightData.MAX_ACCEL[1];
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_Z"]=flightData.MAX_ACCEL[2];

 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_X"]=flightData.RISE_ACC[0];
 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_Y"]=flightData.RISE_ACC[1];
 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_Z"]=flightData.RISE_ACC[2];

 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_X"]=flightData.FALL_ACC[0];
 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_Y"]=flightData.FALL_ACC[1];
 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_Z"]=flightData.FALL_ACC[2];
 
 doc["FLIGHT_STATISTICS"]["MAX_SATALITE"]=flightData.MAX_SIV;
 doc["FLIGHT_STATISTICS"]["ENGINE_BURN_TIME"]=flightData.ENGINEBURN_TIME;
 doc["FLIGHT_STATISTICS"]["APOGEE_TIME"]=flightData.APOGEE_TIME;
 doc["FLIGHT_STATISTICS"]["APOGEE_FALL_SPEED"]=flightData.APOGEE_FALL_SPEED;
 doc["FLIGHT_STATISTICS"]["MAIN_FALL_SPEED"]=flightData.MAIN_FALL_SPEED;
 doc["FLIGHT_STATISTICS"]["DESCENT_TIME"]=flightData.DESCENT_TIME;
 doc["FLIGHT_STATISTICS"]["PYRO_BURN_TIME"]=flightData.pyro_burn_time;
 doc["FLIGHT_STATISTICS"]["FLIGHT_TIME"]=FLIGHT_TIME;
 doc["FLIGHT_STATISTICS"]["MACH_LOCK"]=pyro_stat_arr[4];
 doc["FLIGHT_STATISTICS"]["FLSH_FREE"]=flightData.rocket_flash;
  
  //Serial.println(doc.memoryUsage());
  serializeJson(doc, SerialBT);
  delay(60);
  timeout_bluetooth = millis();
  }
 }


/* 
if(plus==0){fake_alt=fake_alt+10;}
 else if(plus==1){fake_alt=fake_alt-10; if(fake_alt<=10){fake_mode=2;Serial.println("uçuş bitti");}}
 
 if(fake_alt>=3000){plus=1;Serial.println("azalması lazım");}
 else if(fake_alt <= 0){plus=0;}
 
 bt_send=0;
 s1.getBytes(btarray, 2);
 SerialBT.write(btarray[0]);
 delay(20);
 doc.clear();
 doc["ROCKET"]["H3LIS331"]["ACCEL_X"]=flightData.H3LIS331_data[0];
 doc["ROCKET"]["H3LIS331"]["ACCEL_Y"]=flightData.H3LIS331_data[1];
 doc["ROCKET"]["H3LIS331"]["ACCEL_Z"]=flightData.H3LIS331_data[2];

 doc["ROCKET"]["MS5611"]["ALTITUDE"]= fake_alt;//flightData.Ms5611_data[2];
 doc["ROCKET"]["MS5611"]["TEMP"]=flightData.Ms5611_data[0];
 doc["ROCKET"]["MS5611"]["SPEED"]=flightData.Ms5611_data[3];

   doc["ROCKET"]["FLIGHT"]["ID"]=1;
   doc["ROCKET"]["FLIGHT"]["MODE"]=fake_mode;//flight_mode;
 double battt=ReadVoltage(35)*6.6;
 doc["ROCKET"]["BNO055"]["ACCEL_X"]=flightData.bnoAccel[0];
 doc["ROCKET"]["BNO055"]["ACCEL_Y"]=flightData.bnoAccel[1];
 doc["ROCKET"]["BNO055"]["ACCEL_Z"]=flightData.bnoAccel[2];

 doc["ROCKET"]["BNO055"]["GYRO_X"]=flightData.bnoGyro[0];
 doc["ROCKET"]["BNO055"]["GYRO_Y"]=flightData.bnoGyro[1];
 doc["ROCKET"]["BNO055"]["GYRO_Z"]=flightData.bnoGyro[2];

 doc["ROCKET"]["BNO055"]["MAG_X"]=flightData.bnoMag[0];
 doc["ROCKET"]["BNO055"]["MAG_Y"]=flightData.bnoMag[1];
 doc["ROCKET"]["BNO055"]["MAG_Z"]=flightData.bnoMag[2];

 doc["ROCKET"]["BNO055"]["YAW"]=flightData.bnoRaw[0];
 doc["ROCKET"]["BNO055"]["PITCH"]=flightData.bnoRaw[1];
 doc["ROCKET"]["BNO055"]["ROLL"]=flightData.bnoRaw[2];

// 40.081737914676076, 29.5215763658782
//40.08163292506371, 29.519985885472973
 doc["ROCKET"]["GPS"]["LAT"]=40.081632;//flightData.GPS_data[0];
 doc["ROCKET"]["GPS"]["LONG"]=29.519985;//flightData.GPS_data[1];
 doc["ROCKET"]["GPS"]["SAT"]=flightData.SIV;
 doc["ROCKET"]["GPS"]["SPEED"]=flightData.GPS_speed;

 doc["ROCKET"]["BATTERY"]["ROCKET"] = flightData.batteryy;

 //Serial.println("batarya ");
 //Serial.println(battt);
 
 doc["ROCKET"]["BATTERY"]["GS"] = battt;

 doc["ROCKET"]["SD_FREE"] = sd_cap;

 doc["ROCKET"]["LORA"]["RSSI"] = radio.getRSSI();
 
 doc["ROCKET"]["PYRO"]["APOGEE"]=pyro1;
 doc["ROCKET"]["PYRO"]["MAIN"]=pyro2;
 serializeJson(doc, SerialBT);
 delay(60);
*/

}



void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if(!root){
    Serial.println("Failed to open directory");
    return;
  }
  if(!root.isDirectory()){
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while(file){
    if(file.isDirectory()){
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if(levels){
        listDir(fs, file.name(), levels -1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char * path){
  Serial.printf("Creating Dir: %s\n", path);
  if(fs.mkdir(path)){
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char * path){
  Serial.printf("Removing Dir: %s\n", path);
  if(fs.rmdir(path)){
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if(!file){
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while(file.available()){
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, String message){
 // Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file){
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)){
    //  Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
  Serial.printf("Renaming file %s to %s\n", path1, path2);
  if (fs.rename(path1, path2)) {
    Serial.println("File renamed");
  } else {
    Serial.println("Rename failed");
  }
}

void deleteFile(fs::FS &fs, const char * path){
  Serial.printf("Deleting file: %s\n", path);
  if(fs.remove(path)){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}
